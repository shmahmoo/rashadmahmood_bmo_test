public class BinaryTreeNode {
    private int value;
    private BinaryTreeNode rightNode;
    private BinaryTreeNode leftNode;

    public BinaryTreeNode(int value, BinaryTreeNode rightNode, BinaryTreeNode leftNode) {
        this.value = value;
        this.rightNode = rightNode;
        this.leftNode = leftNode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public BinaryTreeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(BinaryTreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public BinaryTreeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(BinaryTreeNode leftNode) {
        this.leftNode = leftNode;
    }

    @Override
    public String toString() {
        String delim = " ";
        StringBuilder outputString = new StringBuilder("");
        outputString.append(this.value + delim);
        return outputString.toString();
    }
}
