public class LinkedList {

    private LinkedListNode headNode;
    private int length = 0;

    public LinkedList() {
    }

     /**
     * Insert sorted in ascending order, allowing duplicates.
     *
     * The complexity of this insertion is O(n) where n is the
     * number of nodes in the linked list. The maximum runtime
     * would be the number of elements in the list.
     */
    public void insert(int newValue) {
        this.length++;

        if (this.headNode == null) {
            this.headNode = new LinkedListNode(newValue, null);
            return;
        }

        if (newValue <= this.headNode.getValue()) {
            LinkedListNode newNode = new LinkedListNode(newValue, this.headNode);
            this.headNode = newNode;
            return;
        }

        LinkedListNode previousNode = this.headNode;
        LinkedListNode nextNode = this.headNode.getNextNode();
        while (nextNode != null) {
            if (newValue <= nextNode.getValue() && newValue >= previousNode.getValue()) {
                LinkedListNode newNode = new LinkedListNode(newValue, nextNode);
                previousNode.setNextNode(newNode);
                return;
            }
            previousNode = nextNode;
            nextNode = nextNode.getNextNode();
        }

        if (newValue <= previousNode.getValue()) {
            LinkedListNode newNode = new LinkedListNode(newValue, previousNode);
            previousNode.setNextNode(newNode);
        } else {
            LinkedListNode newNode = new LinkedListNode(newValue, null);
            previousNode.setNextNode(newNode);
        }
    }

    /**
     *  The complexity of toString() function is O(n) where n is the
     *  number of nodes in the linked list. The runtime would always
     *  be n because the entire linked list needs to be iterated over
     *  to print each node.
     */
    @Override
    public String toString() {
        StringBuilder outputString = new StringBuilder("");
        LinkedListNode currNode = this.headNode;
        while (currNode.getNextNode() != null) {
            outputString.append(currNode.toString());
            currNode = currNode.getNextNode();
        }
        outputString.append(currNode.toString());
        return outputString.toString();
    }

    public int[] toIntArray() {
        int[] nums = new int[this.length];
        LinkedListNode currNode = this.headNode;
        int i = 0;
        while (currNode != null) {
            nums[i] = currNode.getValue();
            i++;
            currNode = currNode.getNextNode();
        }
        return nums;
    }

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        LinkedList linkedList = new LinkedList();
        int[] sortedNumbers = new int[] { -9, 0, 1, 3, 3, 8, 9, 12};
        int[] unsortedNumbers = new int[] { 8, 3, 1, 12, 3, -9, 9, 0};
        for (int number: unsortedNumbers) {
            linkedList.insert(number);
        }

        int[] result = linkedList.toIntArray();

        System.out.printf("\nUnsorted numbers:    %s", Utils.printIntArray(unsortedNumbers));
        System.out.printf("\nSorted numbers:      %s", Utils.printIntArray(sortedNumbers));
        System.out.printf("\nLinked List numbers: %s", Utils.printIntArray(result));

        assert Utils.compareIntArray(sortedNumbers, result) : "test() failed";
    }
}
