public class Utils {
    public static String printIntArray(int[] numbers) {
        StringBuilder output = new StringBuilder("");
        for (int number: numbers) {
            output.append(number);
            output.append(" ");
        }
        return output.toString();
    }

    public static boolean compareIntArray(int[] a, int[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}
