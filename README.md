# Overview

Linked List

The LinkedListNode and LinkedList classes are implementations of a singly linked list which insures numerical order by always inserting
in order. Duplicate values are allowed. The complexity of insertion is O(n) where n is the number of nodes in the collection. The maximum runtime 
would be the time it takes to go through the entire collection. The complexity of the toString() method is O(n) and the 
runtime would always be the time it takes to go through entire collection. 

Binary Tree

The BinaryTreeNode and BinaryTree classes are implementations of binary tree. The complexity of insertion is O(h) where h is the height of 
the tree. The complexity of the toString() method is O(n) where n is the number of nodes in the tree.


# Running

To run the linked list implementation:

```
javac LinkedList.java
java LinkedList

```
To run the binary tree implementation:

```
javac BinaryTreeNode.java
java BinaryTree

```

# Responses to Questions 8


For a linked list the complexity of inserts and searches are both O(n) where n is the number of nodes in the collection.

For a binary tree the complexity of inserts is O(n) where n is the number of nodes in the tree, and the complexity
of searches is O(h) where h is the height of the tree. Expressing h in terms of n we get the following.

n = 2^(h+1) - 1
n + 1 = 2^(h+1)
log2(n+1) = h + 1
....
log2(n) = h

and log2(n) < n

Based on the ratio there are more searches so a binary tree would be the best solution because O(logn) < O(n).

With a tree balancing algorithm the worst case is O(logn).

