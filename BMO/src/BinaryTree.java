public class BinaryTree {

    private BinaryTreeNode rootNode;
    private int length = 0;

    public BinaryTree() {
    }

    private void insert(int newValue, BinaryTreeNode node) {
        if (newValue == node.getValue()) {
            return;
        }

        if (newValue > node.getValue()) {
            if (node.getRightNode() == null) {
                BinaryTreeNode newNode = new BinaryTreeNode(newValue, null, null);
                node.setRightNode(newNode);
                this.length++;
            } else {
                insert(newValue, node.getRightNode());
            }
        }

        if (newValue < node.getValue()) {
            if (node.getLeftNode() == null) {
                BinaryTreeNode newNode = new BinaryTreeNode(newValue, null, null);
                node.setLeftNode(newNode);
                this.length++;
            } else {
                insert(newValue, node.getLeftNode());
            }
        }
    }

    /**
     * Insert, not allowing duplicates.
     *
     * The complexity of this insertion is O(h) where h is the
     * height of the tree. The maximum runtime would be the height
     * of the tree because only 1 element in each level will need
     * to be checked.
     */
    public void insert(int newValue) {
        if (this.rootNode == null) {
            this.rootNode = new BinaryTreeNode(newValue, null, null);
            this.length++;
            return;
        }
        insert(newValue, this.rootNode);
    }

    private void toString(StringBuilder output, BinaryTreeNode currNode) {
        if (currNode.getLeftNode() != null) {
            toString(output, currNode.getLeftNode());
        }
        output.append(currNode.toString());
        if (currNode.getRightNode() != null) {
            toString(output, currNode.getRightNode());
        }
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        toString(output, this.rootNode);
        return output.toString();
    }

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        BinaryTree binaryTree = new BinaryTree();
        int[] sortedNumbers = new int[] { -9, 0, 1, 3, 8, 9, 12, 18};
        int[] unsortedNumbers = new int[] { 8, 3, 1, 12, 3, -9, 9, 0, 18};
        for (int number: unsortedNumbers) {
            binaryTree.insert(number);
        }

        System.out.printf("\nUnsorted numbers:    %s", Utils.printIntArray(unsortedNumbers));
        System.out.printf("\nSorted numbers:      %s", Utils.printIntArray(sortedNumbers));
        System.out.printf("\nBinary tree numbers: %s", binaryTree.toString());
    }
}
