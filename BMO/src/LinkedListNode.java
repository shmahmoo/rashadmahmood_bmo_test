public class LinkedListNode {
    private int value;
    private LinkedListNode nextNode;

    public LinkedListNode(int value, LinkedListNode nextNode) {
        this.value = value;
        this.nextNode = nextNode;
    }

    public LinkedListNode getNextNode() {
        return nextNode;
    }

    public int getValue() {
        return value;
    }

    public void setNextNode(LinkedListNode nextNode) {
        this.nextNode = nextNode;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String delim = " ";
        StringBuilder outputString = new StringBuilder("");
        outputString.append(this.value + delim);
        return outputString.toString();
    }

}
